#### config de git à faire
```
git config --global user.name "<login corp>"
git config --global user.email <email>
git config --global push.default simple
git config --global pull.rebase true
git config --global credential.helper manager
git config --global core.autocrlf true
```

### Reset de la branche pour demo rebase
```shell script
git reset --hard HEAD
git checkout demo_rebase
git reset --hard demo_rebase_before 
```

### Démo rebase sans conflit
```shell script
git reset --hard HEAD
git checkout demo_rebase
git reset --hard demo_rebase_before
git rebase -i demo_rebase_no_conflicts
```

### Démo rebase avec conflit
```shell script
git reset --hard HEAD
git checkout demo_rebase
git reset --hard demo_rebase_before
git rebase -i demo_rebase_with_conflicts
# corriger les conflits
git add -i
git rebase --continue
git push # erreur
git push -f
```

### sabotage de master

```shell script
git reset --hard HEAD
git checkout master
git reset --hard origin/master
cat src/main/resources/resetDemoApp.txt > src/main/java/com/example/demo/DemoApplication.java
cat src/main/resources/resetDemoTest.txt > src/test/java/com/example/demo/DemoApplicationTest.java
```

### MAJ des branches de démo en cas de nouveau master (après push de master)
```shell script
git fetch && \
git reset --hard HEAD && \
git checkout demo_rebase && \
git reset --hard demo_rebase_before && \
git rebase origin/master && \
git push -f && \
git tag -d demo_rebase_before && \
git push origin :demo_rebase_before && \
git tag demo_rebase_before && \
git checkout demo_rebase_with_conflicts && \
git rebase origin/master && \
git push -f && \
git checkout demo_rebase_no_conflicts && \
git rebase origin/master && \
git push -f && \
git checkout master && \
git reset --hard origin/master
```

